import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    images: [],
  },
  getters: {
  },
  mutations: {
    addImage (state, data) {
      state.images.push({src: data[0], orientation: data[1]});
    },
    clearImages (state) {
      while(state.images.length > 0) {
        state.images.pop();
      }
    }
  },
  actions: {
    actionAddImage(context, data) {
      context.commit('addImage', data);
    },
    actionClearImages(context) {
      context.commit('clearImages');
    },
  },
});
